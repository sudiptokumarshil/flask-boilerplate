from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from os import path
from flask_login import LoginManager
from flask_wtf.csrf import CSRFProtect


db = SQLAlchemy()
DB_NAME = 'flask'  # put your database here ...
csrf = CSRFProtect()  # For csrf protection ...


def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = "Hello world"
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:''@localhost/'+DB_NAME
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    db.init_app(app)
    csrf.init_app(app)

    # ================ Starting ======================== (Import your all controller from app)
    # User Auth ...
    from .app.Controllers.User.HomeController import HomeController
    from .app.Controllers.User.AuthController import AuthController
    from .app.Controllers.User.NoteController import NoteController
    # ================ Ending =========================

    # ================= Starting ========================== (Set your base url profix here) ...
    app.register_blueprint(HomeController, url_prefix='/')
    app.register_blueprint(AuthController, url_prefix='/')
    app.register_blueprint(NoteController, url_prefix='/')
    # ================= Ending ============================

    # ================== Starting ========================= (Import your all database model) ...
    from project.models.User import User
    from project.models.Note import Note
    # ================== Ending =========================

    with app.app_context():
        createDatabase()

    login_manager = LoginManager()
    login_manager.login_view = 'AuthController.login'
    login_manager.init_app(app)

    @login_manager.user_loader
    def activeUser(id):
        return User.query.get(int(id))

    return app


def createDatabase():  # (This method will create all existing table automatically) ...
    if not path.exists('project/' + DB_NAME):
        db.create_all()
        print('Created Database!')
