from flask import Blueprint, jsonify, render_template, request, flash, redirect, session, url_for
from project.models.User import User
from werkzeug.security import generate_password_hash, check_password_hash
from .... import db
from flask_login import login_user, login_required, logout_user, current_user


AuthController = Blueprint('AuthController', __name__)


@AuthController.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('HomeController.home'))
    else:
        if request.method == 'POST':
            email = request.form.get('email')
            password = request.form.get('password')

            user = User.query.filter_by(email=email).first()
            if user:
                if check_password_hash(user.password, password):
                    flash('Logged in successfully!', category='success')
                    login_user(user, remember=True)
                    session['userInfo'] = {
                        'id': user.id, 'name': user.name, 'email': user.email}
                    return redirect(url_for('HomeController.home'))
                else:
                    flash('Incorrect password, try again.', category='error')
            else:
                flash('Email does not exist.', category='error')

    return render_template("user/login.html", user=current_user)


@AuthController.route('/logout')
@login_required
def logout():
    logout_user()
    session.pop('userInfo', None)
    return redirect(url_for('AuthController.login'))


@AuthController.route('/signup', methods=['GET', 'POST'])
def sign_up():
    if request.method == 'POST':
        email = request.form.get('email')
        name = request.form.get('name')
        password = request.form.get('password')
        confirm_password = request.form.get('confirm_password')

        user = User.query.filter_by(email=email).first()
        if user:
            flash('Email already exists.', category='error')
        elif len(str(email)) < 4:
            flash('Email must be greater than 3 characters.', category='error')
        elif len(str(name)) < 2:
            flash('First name must be greater than 1 character.', category='error')
        elif password != confirm_password:
            flash('Passwords don\'t match.', category='error')
        elif len(str(password)) < 3:
            flash('Password must be at least 7 characters.', category='error')
        else:
            new_user = User(email=email, name=name, password=generate_password_hash(
                password, method='sha256'))
            db.session.add(new_user)
            db.session.commit()
            flash('Account created!', category='success')
            return redirect(url_for('HomeController.home'))

    return render_template("/user/signup.html", user=current_user)
