from flask import Blueprint, redirect, render_template, url_for
from flask_login import current_user, login_required
HomeController = Blueprint('HomeController', __name__)

@HomeController.route('/')
@login_required
def home():
    if current_user.is_authenticated:
        return render_template("user/home.html", user=current_user)
    return redirect(url_for('AuthController.login'))
