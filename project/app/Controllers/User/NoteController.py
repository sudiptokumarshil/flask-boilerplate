from flask import Blueprint, flash, redirect, render_template, request, session, url_for
from flask_login import current_user, login_required
from project.models.Note import Note
from .... import db
from project.app.Helper.DateTimeHelper import *
from project.app.Helper.BaseHelper import *

NoteController = Blueprint('NoteController', __name__)

dateTime = DateTimeHelper()
baseHelper = BaseHelper()

@NoteController.route('/notes')
@login_required
def index():
    if current_user.is_authenticated:
        return render_template("user/note/index.html", user=current_user)
    return redirect(url_for('AuthController.login'))


@NoteController.route('/note/create', methods=['GET', 'POST'])
def create():
    if current_user.is_authenticated:
        if request.method == 'POST':
            text = request.form.get('text')
            date = request.form.get('date')
            if len(str(text)) < 2:
                flash('Title must be greater than 3 characters.', category='error')
                return render_template("user/note/create.html", user=current_user)
            elif not date:
                flash('Date must not be empty.', category='error')
                return render_template("user/note/create.html", user=current_user)
            else:
                note = Note(text=text, date=date,
                            user_id=baseHelper.active_user())
                db.session.add(note)
                db.session.commit()
                flash('Note created!', category='success')
                return redirect(url_for('NoteController.index'))
        else:
            return render_template("user/note/create.html", user=current_user)
    return redirect(url_for('AuthController.login'))


@NoteController.route('/note/delete', methods=['POST'])
def delete():
    id = request.form.get('id')
    note = db.session().query(Note).get(id)
    if note:
        if note.user_id == baseHelper.active_user():
            db.session().delete(note)
            db.session.commit()
            flash('Note Deleted!', category='success')
            return redirect(url_for('NoteController.index'))
