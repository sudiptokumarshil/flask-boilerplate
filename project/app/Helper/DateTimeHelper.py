import datetime


class DateTimeHelper:
    def get_year_month_date(self, date):
        return datetime.datetime.strptime(date, "%d/%m/%Y").strftime('%Y-%m-%d')

    def get_date_month_year(self, date):
        return datetime.datetime.strptime(date, "%d/%m/%Y").strftime('%Y-%m-%d')
